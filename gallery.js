function getWindowDimensions() {
    var winW = 630, winH = 460;

    // Some sort of cross-browser code to determine window dimensions
    if (document.body && document.body.offsetWidth) {
        winW = document.body.offsetWidth;
        winH = document.body.offsetHeight;
    }
    if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) {
        winW = document.documentElement.offsetWidth;
        winH = document.documentElement.offsetHeight;
    }
    if (window.innerWidth && window.innerHeight) {
        winW = window.innerWidth;
        winH = window.innerHeight;
    }

    return { width: winW, height: winH };
}

function blurScreen() {
    var mask, wrapper, whitewash, spinner, winDims = getWindowDimensions();

    (spinner = document.createElement("img")).id = "spinner";
    spinner.src = "picres/wheel.gif";
    spinner.style.top = (winDims.height - 120) / 2 + "px";
    spinner.style.left = (winDims.width - 120) / 2 + "px";
    document.body.appendChild(spinner);

    (whitewash = document.createElement("div")).id = "whitewash";
    document.body.appendChild(whitewash);

    (mask = document.createElement("div")).id = "mask";
    mask.style.height = winDims.height + "px";
    mask.style.width = winDims.width + "px";
    document.body.appendChild(mask);

    (wrapper = document.createElement("div")).className = "wraptocenter";
    mask.appendChild(wrapper);

    mask.onclick = function () {
        document.body.removeChild(mask);
        document.body.removeChild(whitewash);
        document.body.removeChild(spinner);
    };

    return { wrapper: wrapper };
}

function goFullscreen(initThumbFn) {
    var photosDir = "photos", wrapper, full_img, tray, thumb_img, clicked;

    wrapper = blurScreen().wrapper;

    (full_img = document.createElement("img")).id = "full";
    full_img.src = photosDir + "/" + thumbFns2fullFns[initThumbFn];
    full_img.style.maxHeight = getWindowDimensions().height - 260 + "px";
    full_img.onclick = function () {
        location.href = full_img.src;
    };
    wrapper.appendChild(full_img);

    (tray = document.createElement("div")).id = "tray";
    wrapper.appendChild(tray);

    for (thumbFn in thumbFns2fullFns) {
        (thumb_img = document.createElement("img")).className = "thumb";
        thumb_img.src = photosDir + "/" + thumbFn;
        tray.appendChild(thumb_img);
        if (thumbFn == initThumbFn)
            (clicked = thumb_img).id = "clicked";
        thumb_img.onclick = (function () {
            var fullFn = thumbFns2fullFns[thumbFn]
            return (function (e) {
                try { (e && e.stopPropagation()) || (window.event.cancelBubble = true) } catch(e) { } //stopPropagation returns undefined in Firefox
                full_img.src = photosDir + "/" + fullFn;
                clicked.id = "";
                (clicked = ((e && e.target) || window.event.srcElement)).id = "clicked";
            });
        })();
    }
}

function showMap() {
    var wrapper = blurScreen().wrapper;

    var div = document.createElement("div");
    div.innerHTML = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3316.729585992702!2d151.0693749999999!3d-33.767653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a6a1e95d3f09%3A0x2dcec65b9b09510!2s141+Ray+Rd%2C+Epping+NSW+2121!5e0!3m2!1sen!2sau!4v1442123103011" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>';
    wrapper.appendChild(div);
}

window.onload = function () {
    var preload = new Image();
    preload.src = "picres/wheel.gif";
};
